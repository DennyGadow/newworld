using System;

namespace NewWorld
{
    public static class ConsoleReader
    {
        public static int ReadInt()
        {   
            bool success = false;
            int inputtedInt = 0;

            while(!success)
            {
                string input = Console.ReadLine();
                success = int.TryParse(input, out inputtedInt);
                if(!success) Console.WriteLine("Ungültige Eingabe. Bitte wiederholen.");
            }
            return inputtedInt;
        }
        //more Readers comming soon.
    }
}