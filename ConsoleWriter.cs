using System;
using System.Collections.Generic;
using System.Linq;

namespace NewWorld
{
    public class ConsoleWriter
    {
        private GameManager gm;
        public ConsoleWriter(GameManager gameManager_)
        {
            gm = gameManager_;
        }
        private void WriteInventory()
        {
            Console.WriteLine("Geld: " + gm.Players[0].Money + " | " 
            + gm.wareHouse.GetRessources()[0].RessourceType + ": " 
            + gm.wareHouse.GetPlayerRessourceStockPile(gm.Players[0],
            gm.wareHouse.GetRessources()[0])+ "\n");

            Console.WriteLine($"{"", 8}{"Vorrat", 8}{"Preis", 8}");

            for(int ressCount = 1; ressCount < 5; ressCount++)
            {
                string ressType = gm.wareHouse.GetRessources()[ressCount].RessourceType;
                string playerStockPile = gm.wareHouse.GetPlayerRessourceStockPile(gm.Players[0],
                gm.wareHouse.GetRessources()[ressCount]).ToString();
                string ressPrice = gm.wareHouse.GetRessources()[ressCount].Price.ToString();
                Console.WriteLine($"{ressType, -8}{playerStockPile, 8}{ressPrice, 8}");
                
            }

            Console.WriteLine("\n---\n");
        }

        public void WriteMainMenu()
        {
            WriteInventory();
            Console.WriteLine("1. Land verwalten\n2. Markt besuchen\n3. Runde beenden");
        }

        public void WriteLandMenu()
        {
            WriteInventory();
            Console.WriteLine("1. Land kaufen\n2. Land verkaufen\n3. Zurück");
        }
        public void WriteMarketMenu()
        {
            WriteInventory();
            Console.WriteLine("1. Vorräte kaufen\n2. Vorräte verkaufen\n3. Zurück");
        }

        public void WriteDealSuppliesMenu(int sellBuyState)
        {
            WriteInventory();

            string dealToken = sellBuyState == 0 ? "gekauft" : "verkauft";

            Console.WriteLine("Welche Vorräte sollen " + dealToken + " werden?");

            for(int ressCount = 1; ressCount < 5; ressCount++)
                Console.WriteLine(ressCount + ". " + gm.wareHouse.GetRessources()[ressCount].RessourceType);
            
            Console.WriteLine("5. Abbrechen");
        }

        public void WriteBuyMenu(int ressource)
        {
            Console.Write("\nWie viel " + gm.wareHouse.GetRessources()[ressource].RessourceType + " soll gekauft werden? ");
            
            if(!gm.Market.Buy(gm.Players[0], gm.wareHouse.GetRessources()[ressource], ConsoleReader.ReadInt()))
                Console.WriteLine("Nicht genug Geld vorhanden!");
        }

        public void WriteSellMenu(int ressource)
        {
            Console.Write("\nWie viel " + gm.wareHouse.GetRessources()[ressource].RessourceType + " soll verkauft werden? ");

            if(!gm.Market.Sell(gm.Players[0], gm.wareHouse.GetRessources()[ressource], ConsoleReader.ReadInt()))
                Console.WriteLine("Nicht genug " + gm.wareHouse.GetRessources()[ressource].RessourceType
                 + " vorhanden!");
        }
    }
}