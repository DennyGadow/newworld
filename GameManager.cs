using System;
using System.Collections.Generic;
using System.Linq;

namespace NewWorld
{
    public class GameManager
    {
        public List <Player> Players { get; }     
        public IWareHouse wareHouse { get; }
        public Market Market { get; }
        private StateMachine stateMachine; 
        public GameManager(IWareHouse wareHouse_)
        {
            Ressource Land = new Ressource("Land", 50);
            Ressource Grain = new Ressource("Getreide", 25);
            Ressource oil = new Ressource("Öl", 50);
            Ressource metall = new Ressource("Metall", 60);
            Ressource crystal = new Ressource("Kristall", 75);

            wareHouse = wareHouse_;
            wareHouse.AddRessource(Land, 100, 5);
            wareHouse.AddRessource(Grain, 1000, 25);
            wareHouse.AddRessource(oil, 1000, 10);
            wareHouse.AddRessource(metall, 1000, 5);
            wareHouse.AddRessource(crystal, 1000, 0);
            
            Players = new List<Player>();
            Player player1 = new Player("Denny", 100);
            wareHouse.RegisterPlayer(player1);
            Players.Add(player1);        

            Market = new Market(wareHouse);

            stateMachine = new StateMachine(this);            
            stateMachine.Run();
        }
    }
}