using System.Collections.Generic;

namespace NewWorld
{
    public interface IWareHouse
    {
        void AddRessource(Ressource ressource, int marketStockPile, int playerStockPile);
        void RegisterPlayer(Player player);
        List<Ressource> GetRessources();
        int GetMarketStockPile(Ressource ressource);
        int GetPlayerRessourceStockPile(Player player, Ressource ressource);
        void IncreaseMarketStockPile(Ressource ressource, int amount);
        void DecreaseMarketStockPile(Ressource ressource, int amount);
        void InreasePlayerStockPile(Player player, Ressource ressource, int amount);
        void DecreasePlayerStockPile(Player player, Ressource ressource, int amount);
    }
}