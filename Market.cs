using System;
using System.Collections.Generic;
using System.Linq;

namespace NewWorld
{
    public class Market
    {
        private float returnValue = 0.9f;
        private IWareHouse wareHouse;

        public Market(IWareHouse wareHouse_)
        {
            wareHouse = wareHouse_;
        }
        public bool Buy(Player player, Ressource ressource, int amount)
        {
            if(player.Money >= ressource.Price * amount && wareHouse.GetMarketStockPile(ressource) >= amount)
            {
                player.Money -= ressource.Price * amount;
                wareHouse.InreasePlayerStockPile(player, ressource, amount);
                wareHouse.DecreaseMarketStockPile(ressource, amount);
                return true;
            }
            else
                return false;            
        }

        public bool Sell(Player player, Ressource ressource, int amount)
        {
            if(wareHouse.GetPlayerRessourceStockPile(player, ressource) >= amount)
            {
                wareHouse.DecreasePlayerStockPile(player, ressource, amount);
                wareHouse.IncreaseMarketStockPile(ressource, amount);              
                player.Money += (float)Math.Floor(returnValue * ressource.Price * amount);
                return true;
            }
            else
                return false;
        }
    }
}