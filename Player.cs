using System;
using System.Linq;
using System.Collections.Generic;

namespace NewWorld
{
    public class Player
    {
        public string Name { get; }
        public float Money { get; set; }

        public Player(String name, int money)
        {
            Name = name;
            Money = money;
        }
    }
}