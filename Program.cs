﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NewWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            IWareHouse wareHouse = new WareHouse();
            GameManager gameManager = new GameManager(wareHouse);
        }
    }
}
