using System;

namespace NewWorld
{
    public class Ressource
    {
        public string RessourceType { get; set; }
        public float Price { get; set; }
                
        public Ressource(string ressourceType, float price)
        {
            RessourceType = ressourceType;
            Price = price;
        }
    }    
}