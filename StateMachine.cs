using System;

namespace NewWorld
{
    public class StateMachine
    {
        private ConsoleWriter cW;
        
        public StateMachine(GameManager gm)
        {
            cW = new ConsoleWriter(gm);
        }

        public void Run()
        {
            MainMenuStates();
        }

        private void MainMenuStates()
        {
            cW.WriteMainMenu();
            int input = ConsoleReader.ReadInt();

            switch (input)
            {
                case 1:
                    LandStates();
                    break;
                case 2:
                    MarketStates();
                    break;
                case 3:
                    //exit
                    break;
                default:
                    Console.WriteLine("Ungültige Eingabe. Bitte wiederholen.");
                    MainMenuStates();
                    break;
            }
        }

        private void LandStates()
        {
            cW.WriteLandMenu();
            int input = ConsoleReader.ReadInt();

            switch (input)
            {
                case 1:
                    cW.WriteBuyMenu(0);
                    LandStates();
                    break;
                case 2:
                    cW.WriteSellMenu(0);
                    LandStates();
                    break;
                case 3:
                    MainMenuStates();
                    break;
                default:
                    Console.WriteLine("Ungültige Eingabe. Bitte wiederholen.");
                    LandStates();
                    break;
            }
        }

        private void MarketStates()
        {
            cW.WriteMarketMenu();
            int input = ConsoleReader.ReadInt();

            switch (input)
            {
                case 1:
                    cW.WriteDealSuppliesMenu(0);
                    SupplyStates(0);
                    break;
                case 2:
                    cW.WriteDealSuppliesMenu(1);
                    SupplyStates(1);
                    break;
                case 3:
                    MainMenuStates();
                    break;
                default:
                    Console.WriteLine("Ungültige Eingabe. Bitte wiederholen.");
                    MarketStates();
                    break;
            }
        }

        private void SupplyStates(int buySellState)
        {
            int input = ConsoleReader.ReadInt();

            if(buySellState == 0)
            {
                switch (input)
                {
                    case 1:
                        cW.WriteBuyMenu(1);
                        MarketStates();
                        break;
                    case 2:
                        cW.WriteBuyMenu(2);
                        MarketStates();
                        break;
                    case 3:
                        cW.WriteBuyMenu(3);
                        MarketStates();
                        break;
                    case 4:
                        cW.WriteBuyMenu(4);
                        MarketStates();
                        break;
                    case 5:
                        MarketStates();
                        break;
                    default:
                        Console.WriteLine("Ungültige Eingabe. Bitte wiederholen.");
                        SupplyStates(buySellState);
                        break;
                }
            }

            if(buySellState == 1)
            {
                switch (input)
                {
                    case 1:
                        cW.WriteSellMenu(1);
                        MarketStates();
                        break;
                    case 2:
                        cW.WriteSellMenu(2);
                        MarketStates();
                        break;
                    case 3:
                        cW.WriteSellMenu(3);
                        MarketStates();
                        break;
                    case 4:
                        cW.WriteSellMenu(4);
                        MarketStates();
                        break;
                    case 5:
                        MarketStates();
                        break;
                    default:
                        Console.WriteLine("Ungültige Eingabe. Bitte wiederholen.");
                        SupplyStates(buySellState);
                        break;
                }
            }
        }
    }
}