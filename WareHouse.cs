using System.Collections.Generic;

namespace NewWorld
{
    public class WareHouse : IWareHouse
    {
        private Dictionary<Ressource, int> exemplaryPlayerInventory;

        private List<Ressource> Ressources;
        private Dictionary<Ressource, int> MarketOffers;
        private Dictionary<Player, Dictionary<Ressource, int>> PlayerInventories;

        public WareHouse()
        {
            exemplaryPlayerInventory = new Dictionary<Ressource, int>();
            Ressources = new List<Ressource>();
            MarketOffers = new Dictionary<Ressource, int>();
            PlayerInventories = new Dictionary<Player, Dictionary<Ressource, int>>();
        }
        
        public List<Ressource> GetRessources() { return Ressources; }

        public void AddRessource(Ressource ressource, int marketStockPile, int playerStockPile)
        {
            Ressources.Add(ressource);
            MarketOffers.Add(ressource, marketStockPile);
            exemplaryPlayerInventory.Add(ressource, playerStockPile);
        }

        public void RegisterPlayer(Player player)
        {
            PlayerInventories.Add(player, new Dictionary<Ressource,int>(exemplaryPlayerInventory));
        }

        public int GetMarketStockPile(Ressource ressource)
        {
            return MarketOffers[ressource];
        }

        public int GetPlayerRessourceStockPile(Player player, Ressource ressource)
        {
            return PlayerInventories[player][ressource];
        }

        public void IncreaseMarketStockPile(Ressource ressource, int amount)
        {
            MarketOffers[ressource] += amount;
        }

        public void DecreaseMarketStockPile(Ressource ressource, int amount)
        {
            MarketOffers[ressource] -= amount;
        }

        public void InreasePlayerStockPile(Player player, Ressource ressource, int amount)
        {
            PlayerInventories[player][ressource] += amount;
        }

        public void DecreasePlayerStockPile(Player player, Ressource ressource, int amount)
        {
            PlayerInventories[player][ressource] -= amount;
        }
    }
}